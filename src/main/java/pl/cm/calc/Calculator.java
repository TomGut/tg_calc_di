package pl.cm.calc;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import pl.cm.calc.calculation.CalculationProcessor;
import pl.cm.calc.io.InputReader;
import pl.cm.calc.io.NewStar;
import pl.cm.calc.io.OutputWriter;
import pl.cm.calc.io.SimpleOutputWriter;
import pl.cm.calc.model.UserInputDto;

@Component
public class Calculator {
  private final InputReader inputReader;
  private final CalculationProcessor calculationProcessor;
  private final OutputWriter outputWriter;

  @Autowired
  public Calculator(InputReader inputReader, @Qualifier("sow") OutputWriter outputWriter, CalculationProcessor calculationProcessor) {
    this.inputReader = inputReader;
    this.outputWriter = outputWriter;
    this.calculationProcessor = calculationProcessor;
  }

  public void start() {
    final UserInputDto userInput = inputReader.getUserInput();
    final Optional<Long> result = calculationProcessor.calculate(userInput.getOperation(), userInput.getLeftOperand(), userInput.getRightOperand());

    if (result.isPresent()) {
      outputWriter.write("Wynik obliczen: " + result.get());
    } else {
      outputWriter.write("Niepoprawne dane wejsciowe ");
    }
  }
}
