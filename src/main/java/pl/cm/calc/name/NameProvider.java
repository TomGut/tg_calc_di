package pl.cm.calc.name;

public abstract class NameProvider implements HasName {

  private final String name;

  public NameProvider(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }
}
