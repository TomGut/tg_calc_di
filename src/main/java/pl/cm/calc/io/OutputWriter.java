package pl.cm.calc.io;

public interface OutputWriter {
  public void write(String message);
}
