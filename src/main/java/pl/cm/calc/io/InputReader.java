package pl.cm.calc.io;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.cm.calc.model.UserInputDto;

@Component
public class InputReader {

  private final OutputWriter outputWriter;

  @Autowired
  public InputReader(final OutputWriter outputWriter) {
    this.outputWriter = outputWriter;
  }

  public UserInputDto getUserInput() {
    Scanner scanner = new Scanner(System.in);
    outputWriter.write("Wprowadz typ operacji");
    final String operation = scanner.next();
    outputWriter.write("Wprowadz pierwsza liczbe");
    final long firstNumber = scanner.nextLong();
    outputWriter.write("Wprowadz druga liczbe");
    final long secondNumber = scanner.nextLong();
    return new UserInputDto(operation, firstNumber, secondNumber);
  }

}
