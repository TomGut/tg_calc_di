package pl.cm.calc.io;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class NewStar implements OutputWriter {

    @Override
    public void write(String message) {
        System.out.println("* " + message);
    }
}
