package pl.cm.calc.io;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component("sow")
public class SimpleOutputWriter implements OutputWriter {

  @Override
  public void write(final String message) {
    System.out.println(message);
  }
}
