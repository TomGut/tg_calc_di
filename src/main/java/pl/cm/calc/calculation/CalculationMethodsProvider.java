package pl.cm.calc.calculation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

@Component
public class CalculationMethodsProvider {
  private List<Calculation> calculations = new ArrayList<>();

  @Autowired
  public CalculationMethodsProvider(List<Calculation> calculations) {
    this.calculations = calculations;
  }

  public List<Calculation> getAvailableCalculationMethods() {
    return this.calculations;
  }
}
