package pl.cm.calc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import pl.cm.calc.calculation.CalculationProcessor;
import pl.cm.calc.io.InputReader;
import pl.cm.calc.io.OutputWriter;
import pl.cm.calc.model.UserInputDto;

public class CalculatorTest {
  private final String operation = "add";
  private final long leftOperand = 10;
  private final long rightOperand = 16;
  private final UserInputDto inputDto = new UserInputDto(operation, leftOperand, rightOperand);

  @Mock
  private InputReader inputReader;
  @Mock
  private OutputWriter outputWriter;
  @Mock
  private CalculationProcessor calculationProcessor;
  @Captor
  private ArgumentCaptor<String> outputCaptor;

  @InjectMocks
  private Calculator calculator;

  @Before
  public void before() throws Exception {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void should_return_unsupported_operation_given_wrong_operation_name() {
    //static import of Mockito.when
    when(inputReader.getUserInput()).thenReturn(inputDto);
    when(calculationProcessor.calculate(eq(operation), eq(leftOperand), eq(rightOperand))).thenReturn(Optional.empty());

    calculator.start();

    verify(outputWriter).write("Niepoprawne dane wejsciowe ");
  }

  @Test
  public void should_return_calculation_result() {
    //static import of Mockito.when
    when(inputReader.getUserInput()).thenReturn(inputDto);
    when(calculationProcessor.calculate(eq(operation), eq(leftOperand), eq(rightOperand))).thenReturn(Optional.of(20l));

    calculator.start();

    verify(outputWriter, times(1)).write(outputCaptor.capture());
    final String outputString = outputCaptor.getValue();
    assertThat(outputString).contains("20");
  }


}